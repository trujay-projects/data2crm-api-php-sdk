<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\EventDescribe;
use Data2CRMAPI\Model\EventEntity;
use Data2CRMAPI\Model\EventEntityRelation;
use Data2CRMAPI\Model\Count;

class EventApi extends AbstractApi
{
    const HAS_QUERY_FILTER = true;
    const HAS_QUERY_FIELDS = true;
    
    /**
     * @var string
     */
    protected $path = '/event';

    /**
     * @return EventDescribe
     */
    public function describe()
    {
        return $this->doDescribe('\Data2CRMAPI\Model\EventDescribe');
    }

    /**
     * @return Count
     */
    public function count()
    {
        return $this->doCount('\Data2CRMAPI\Model\Count');
    }

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $filter
     * @param array $fields
     * 
     * @return EventEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $filter = array(), array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, $filter, $fields, '\Data2CRMAPI\Model\EventEntity[]');
    }

    /**
     * @param string $id
     * 
     * @return EventEntity
     */
    public function fetch($id)
    {
        return $this->doFetch($id, '\Data2CRMAPI\Model\EventEntity');
    }

    /**
     * @param EventEntity $event
     *
     * @return EventEntityRelation
     */
    public function create(EventEntity $event)
    {
        return $this->doCreate($event, '\Data2CRMAPI\Model\EventEntityRelation');
    }

    /**
     * @param string $id
     * @param EventEntity $event
     * 
     * @return EventEntityRelation
     */
    public function update($id, EventEntity $event)
    {
        return parent::doUpdate($id, $event, '\Data2CRMAPI\Model\EventEntityRelation');
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        parent::doDelete($id);
    }
}
