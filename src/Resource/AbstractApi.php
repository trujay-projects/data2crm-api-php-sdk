<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\ApiClient;
use Data2CRMAPI\ApiException;
use Data2CRMAPI\Data;
use Data2CRMAPI\Model\AbstractModel;
use Data2CRMAPI\ObjectSerializer;

abstract class AbstractApi
{
    const HAS_QUERY_FILTER = null;
    const HAS_QUERY_FIELDS = null;

    /**
     * API Client
     *
     * @var ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Path
     *
     * @var string
     */
    protected $path;

    /**
     * @var Data
     */
    protected $data;

    /**
     * Object Serializer
     *
     * @var ObjectSerializer
     */
    protected $serializer;

    /**
     * Constructor
     *
     * @param ApiClient $apiClient The api client to use
     */
    public function __construct(ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        $this->data = new Data();
        $this->serializer = new ObjectSerializer();
    }

    /**
     * Get API client
     *
     * @return ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param ApiClient $apiClient set the API client
     *
     * @return AbstractApi
     */
    public function setApiClient(ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * @return Data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param Data $data
     */
    public function setData(Data $data)
    {
        $this->data = $data;
    }

    /**
     * @param $type
     *
     * @return AbstractModel
     *
     * @throws ApiException
     */
    protected function doDescribe($type)
    {
        return $this->getApiClient()->call(
            $this->path . '/describe', 
            'GET',
            array(),
            array(),
            array(),
            $type,
            $this->data
        );
    }

    /**
     * @param string $type
     *
     * @return AbstractModel
     *
     * @throws ApiException
     */
    protected function doCount($type)
    {
        return $this->getApiClient()->call(
            $this->path . '/count',
            'GET',
            array(),
            array(),
            array(),
            $type,
            $this->data
        );
    }

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $filter
     * @param array $fields
     * @param string $type
     *
     * @return AbstractModel
     *
     * @throws ApiException
     */
    protected function doFetchAll($pageSize = null, $page = null, array $filter = array(), array $fields = array(), $type)
    {
        $query = array();

        if (null !== $pageSize) {
            $query['page_size'] = $this->apiClient->getSerializer()->toQueryValue($pageSize);
        }

        if (null !== $page) {
            $query['page'] = $this->apiClient->getSerializer()->toQueryValue($page);
        }

        if (true === static::HAS_QUERY_FILTER && false === empty($filter)) {
            $query['filter'] = $this->apiClient->getSerializer()->toQueryValue(json_encode($filter));
        }

        if (true === static::HAS_QUERY_FIELDS && false === empty($fields)) {
            $query['fields'] = $this->apiClient->getSerializer()->toQueryValue(implode(',', $fields));
        }
        
        $headers = array();
        
        if (true === isset($query['filter'])) {
            $headers[Data::HEADER_ENABLE] = Data::ENABLE_TRUE;
        }
        
        return $this->getApiClient()->call(
            $this->path,
            'GET',
            $query,
            array(),
            $headers,
            $type,
            $this->data
        );
    }

    /**
     * @param string $id
     * @param string $type
     *
     * @return AbstractModel
     *
     * @throws ApiException
     */
    protected function doFetch($id, $type)
    {
        return $this->getApiClient()->call(
            $this->path . '/' . $id,
            'GET',
            array(),
            array(),
            array(),
            $type,
            $this->data
        );
    }

    /**
     * @param AbstractModel $model
     * @param string $type
     * 
     * @return AbstractModel
     * 
     * @throws ApiException
     */
    protected function doCreate(AbstractModel $model, $type)
    {
        return $this->getApiClient()->call(
            $this->path,
            'POST',
            array(),
            $this->prepare($model),
            array(),
            $type,
            $this->data
        );
    }

    /**
     * @param AbstractModel $model
     * @param string $id
     * @param string $type
     *
     * @return AbstractModel
     *
     * @throws ApiException
     */
    protected function doUpdate($id, AbstractModel $model, $type)
    {
        return $this->getApiClient()->call(
            $this->path . '/' . $id,
            'PUT', 
            array(),
            $this->prepare($model),
            array(),
            $type,
            $this->data
        );
    }

    /**
     * @param string $id
     * 
     * @throws ApiException
     */
    protected function doDelete($id)
    {
        $this->getApiClient()->call(
            $this->path . '/' . $id,
            'DELETE',
            array(),
            array(),
            array(),
            null,
            $this->data
        );
    }

    /**
     * @param AbstractModel $model
     * 
     * @return array
     */
    protected function prepare(AbstractModel $model)
    {
        return array_merge(
            json_decode(
                json_encode(
                    $this->serializer->sanitizeForSerialization($model)
                ), 
                true
            ),
            $model->getRaw()
        );
    }
}