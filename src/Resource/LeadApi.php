<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\LeadDescribe;
use Data2CRMAPI\Model\LeadEntity;
use Data2CRMAPI\Model\LeadEntityRelation;
use Data2CRMAPI\Model\Count;

class LeadApi extends AbstractApi
{
    const HAS_QUERY_FILTER = true;
    const HAS_QUERY_FIELDS = true;
    
    /**
     * @var string
     */
    protected $path = '/lead';

    /**
     * @return LeadDescribe
     */
    public function describe()
    {
        return $this->doDescribe('\Data2CRMAPI\Model\LeadDescribe');
    }

    /**
     * @return Count
     */
    public function count()
    {
        return $this->doCount('\Data2CRMAPI\Model\Count');
    }

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $filter
     * @param array $fields
     * 
     * @return LeadEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $filter = array(), array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, $filter, $fields, '\Data2CRMAPI\Model\LeadEntity[]');
    }

    /**
     * @param string $id
     * 
     * @return LeadEntity
     */
    public function fetch($id)
    {
        return $this->doFetch($id, '\Data2CRMAPI\Model\LeadEntity');
    }

    /**
     * @param LeadEntity $lead
     *
     * @return LeadEntityRelation
     */
    public function create(LeadEntity $lead)
    {
        return $this->doCreate($lead, '\Data2CRMAPI\Model\LeadEntityRelation');
    }

    /**
     * @param string $id
     * @param LeadEntity $lead
     * 
     * @return LeadEntityRelation
     */
    public function update($id, LeadEntity $lead)
    {
        return parent::doUpdate($id, $lead, '\Data2CRMAPI\Model\LeadEntityRelation');
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        parent::doDelete($id);
    }
}
