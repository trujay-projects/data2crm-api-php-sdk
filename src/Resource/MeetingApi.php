<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\MeetingDescribe;
use Data2CRMAPI\Model\MeetingEntity;
use Data2CRMAPI\Model\MeetingEntityRelation;
use Data2CRMAPI\Model\Count;

class MeetingApi extends AbstractApi
{
    const HAS_QUERY_FILTER = true;
    const HAS_QUERY_FIELDS = true;
    
    /**
     * @var string
     */
    protected $path = '/meeting';

    /**
     * @return MeetingDescribe
     */
    public function describe()
    {
        return $this->doDescribe('\Data2CRMAPI\Model\MeetingDescribe');
    }

    /**
     * @return Count
     */
    public function count()
    {
        return $this->doCount('\Data2CRMAPI\Model\Count');
    }

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $filter
     * @param array $fields
     * 
     * @return MeetingEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $filter = array(), array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, $filter, $fields, '\Data2CRMAPI\Model\MeetingEntity[]');
    }

    /**
     * @param string $id
     * 
     * @return MeetingEntity
     */
    public function fetch($id)
    {
        return $this->doFetch($id, '\Data2CRMAPI\Model\MeetingEntity');
    }

    /**
     * @param MeetingEntity $meeting
     *
     * @return MeetingEntityRelation
     */
    public function create(MeetingEntity $meeting)
    {
        return $this->doCreate($meeting, '\Data2CRMAPI\Model\MeetingEntityRelation');
    }

    /**
     * @param string $id
     * @param MeetingEntity $meeting
     * 
     * @return MeetingEntityRelation
     */
    public function update($id, MeetingEntity $meeting)
    {
        return parent::doUpdate($id, $meeting, '\Data2CRMAPI\Model\MeetingEntityRelation');
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        parent::doDelete($id);
    }
}
