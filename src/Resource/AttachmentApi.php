<?php

namespace Data2CRMAPI\Resource;

use Data2CRMAPI\Model\AttachmentDescribe;
use Data2CRMAPI\Model\AttachmentEntity;
use Data2CRMAPI\Model\AttachmentEntityRelation;
use Data2CRMAPI\Model\Count;

class AttachmentApi extends AbstractApi
{
    const HAS_QUERY_FILTER = true;
    const HAS_QUERY_FIELDS = true;
    
    /**
     * @var string
     */
    protected $path = '/attachment';

    /**
     * @return AttachmentDescribe
     */
    public function describe()
    {
        return $this->doDescribe('\Data2CRMAPI\Model\AttachmentDescribe');
    }

    /**
     * @return Count
     */
    public function count()
    {
        return $this->doCount('\Data2CRMAPI\Model\Count');
    }

    /**
     * @param null|int $pageSize
     * @param null|int $page
     * @param array $filter
     * @param array $fields
     * 
     * @return AttachmentEntity[]
     */
    public function fetchAll($pageSize = null, $page = null, array $filter = array(), array $fields = array())
    {
        return $this->doFetchAll($pageSize, $page, $filter, $fields, '\Data2CRMAPI\Model\AttachmentEntity[]');
    }

    /**
     * @param string $id
     * 
     * @return AttachmentEntity
     */
    public function fetch($id)
    {
        return $this->doFetch($id, '\Data2CRMAPI\Model\AttachmentEntity');
    }

    /**
     * @param AttachmentEntity $attachment
     *
     * @return AttachmentEntityRelation
     */
    public function create(AttachmentEntity $attachment)
    {
        return $this->doCreate($attachment, '\Data2CRMAPI\Model\AttachmentEntityRelation');
    }

    /**
     * @param string $id
     * @param AttachmentEntity $attachment
     * 
     * @return AttachmentEntityRelation
     */
    public function update($id, AttachmentEntity $attachment)
    {
        return parent::doUpdate($id, $attachment, '\Data2CRMAPI\Model\AttachmentEntityRelation');
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        parent::doDelete($id);
    }
}
