<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class Relation extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'account' => '\Data2CRMAPI\Model\AccountRelation[]',
        'contact' => '\Data2CRMAPI\Model\ContactRelation[]',
        'lead' => '\Data2CRMAPI\Model\LeadRelation[]',
        'opportunity' => '\Data2CRMAPI\Model\OpportunityRelation[]',
        'task' => '\Data2CRMAPI\Model\TaskRelation[]',
        'call' => '\Data2CRMAPI\Model\CallRelation[]',
        'email' => '\Data2CRMAPI\Model\EmailRelation[]',
        'event' => '\Data2CRMAPI\Model\EventRelation[]',
        'meeting' => '\Data2CRMAPI\Model\MeetingRelation[]',
        'note' => '\Data2CRMAPI\Model\NoteRelation[]',
        'attachment' => '\Data2CRMAPI\Model\AttachmentRelation[]',
        'campaign' => '\Data2CRMAPI\Model\CampaignRelation[]',
        'project' => '\Data2CRMAPI\Model\ProjectRelation[]',
        'case' => '\Data2CRMAPI\Model\CaseRelation[]'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'account' => 'account',
        'contact' => 'contact',
        'lead' => 'lead',
        'opportunity' => 'opportunity',
        'task' => 'task',
        'call' => 'call',
        'email' => 'email',
        'event' => 'event',
        'meeting' => 'meeting',
        'note' => 'note',
        'attachment' => 'attachment',
        'campaign' => 'campaign',
        'project' => 'project',
        'case' => 'case'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'account' => 'setAccount',
        'contact' => 'setContact',
        'lead' => 'setLead',
        'opportunity' => 'setOpportunity',
        'task' => 'setTask',
        'call' => 'setCall',
        'email' => 'setEmail',
        'event' => 'setEvent',
        'meeting' => 'setMeeting',
        'note' => 'setNote',
        'attachment' => 'setAttachment',
        'campaign' => 'setCampaign',
        'project' => 'setProject',
        'case' => 'setCase'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'account' => 'getAccount',
        'contact' => 'getContact',
        'lead' => 'getLead',
        'opportunity' => 'getOpportunity',
        'task' => 'getTask',
        'call' => 'getCall',
        'email' => 'getEmail',
        'event' => 'getEvent',
        'meeting' => 'getMeeting',
        'note' => 'getNote',
        'attachment' => 'getAttachment',
        'campaign' => 'getCampaign',
        'project' => 'getProject',
        'case' => 'getCase'
    );

    /**
     * Gets account
     *
     * @return \Data2CRMAPI\Model\AccountRelation[]
     */
    public function getAccount()
    {
        return $this->offsetGet('account');
    }

    /**
     * Sets account
     *
     * @param \Data2CRMAPI\Model\AccountRelation[] $account Account
     *
     * @return $this
     */
    public function setAccount($account)
    {
        $this->offsetSet('account', $account);

        return $this;
    }
    /**
     * Gets contact
     *
     * @return \Data2CRMAPI\Model\ContactRelation[]
     */
    public function getContact()
    {
        return $this->offsetGet('contact');
    }

    /**
     * Sets contact
     *
     * @param \Data2CRMAPI\Model\ContactRelation[] $contact Contact
     *
     * @return $this
     */
    public function setContact($contact)
    {
        $this->offsetSet('contact', $contact);

        return $this;
    }
    /**
     * Gets lead
     *
     * @return \Data2CRMAPI\Model\LeadRelation[]
     */
    public function getLead()
    {
        return $this->offsetGet('lead');
    }

    /**
     * Sets lead
     *
     * @param \Data2CRMAPI\Model\LeadRelation[] $lead Lead
     *
     * @return $this
     */
    public function setLead($lead)
    {
        $this->offsetSet('lead', $lead);

        return $this;
    }
    /**
     * Gets opportunity
     *
     * @return \Data2CRMAPI\Model\OpportunityRelation[]
     */
    public function getOpportunity()
    {
        return $this->offsetGet('opportunity');
    }

    /**
     * Sets opportunity
     *
     * @param \Data2CRMAPI\Model\OpportunityRelation[] $opportunity Opportunity
     *
     * @return $this
     */
    public function setOpportunity($opportunity)
    {
        $this->offsetSet('opportunity', $opportunity);

        return $this;
    }
    /**
     * Gets task
     *
     * @return \Data2CRMAPI\Model\TaskRelation[]
     */
    public function getTask()
    {
        return $this->offsetGet('task');
    }

    /**
     * Sets task
     *
     * @param \Data2CRMAPI\Model\TaskRelation[] $task Task
     *
     * @return $this
     */
    public function setTask($task)
    {
        $this->offsetSet('task', $task);

        return $this;
    }
    /**
     * Gets call
     *
     * @return \Data2CRMAPI\Model\CallRelation[]
     */
    public function getCall()
    {
        return $this->offsetGet('call');
    }

    /**
     * Sets call
     *
     * @param \Data2CRMAPI\Model\CallRelation[] $call Call
     *
     * @return $this
     */
    public function setCall($call)
    {
        $this->offsetSet('call', $call);

        return $this;
    }
    /**
     * Gets email
     *
     * @return \Data2CRMAPI\Model\EmailRelation[]
     */
    public function getEmail()
    {
        return $this->offsetGet('email');
    }

    /**
     * Sets email
     *
     * @param \Data2CRMAPI\Model\EmailRelation[] $email Email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->offsetSet('email', $email);

        return $this;
    }
    /**
     * Gets event
     *
     * @return \Data2CRMAPI\Model\EventRelation[]
     */
    public function getEvent()
    {
        return $this->offsetGet('event');
    }

    /**
     * Sets event
     *
     * @param \Data2CRMAPI\Model\EventRelation[] $event Event
     *
     * @return $this
     */
    public function setEvent($event)
    {
        $this->offsetSet('event', $event);

        return $this;
    }
    /**
     * Gets meeting
     *
     * @return \Data2CRMAPI\Model\MeetingRelation[]
     */
    public function getMeeting()
    {
        return $this->offsetGet('meeting');
    }

    /**
     * Sets meeting
     *
     * @param \Data2CRMAPI\Model\MeetingRelation[] $meeting Meeting
     *
     * @return $this
     */
    public function setMeeting($meeting)
    {
        $this->offsetSet('meeting', $meeting);

        return $this;
    }
    /**
     * Gets note
     *
     * @return \Data2CRMAPI\Model\NoteRelation[]
     */
    public function getNote()
    {
        return $this->offsetGet('note');
    }

    /**
     * Sets note
     *
     * @param \Data2CRMAPI\Model\NoteRelation[] $note Note
     *
     * @return $this
     */
    public function setNote($note)
    {
        $this->offsetSet('note', $note);

        return $this;
    }
    /**
     * Gets attachment
     *
     * @return \Data2CRMAPI\Model\AttachmentRelation[]
     */
    public function getAttachment()
    {
        return $this->offsetGet('attachment');
    }

    /**
     * Sets attachment
     *
     * @param \Data2CRMAPI\Model\AttachmentRelation[] $attachment Attachment
     *
     * @return $this
     */
    public function setAttachment($attachment)
    {
        $this->offsetSet('attachment', $attachment);

        return $this;
    }
    /**
     * Gets campaign
     *
     * @return \Data2CRMAPI\Model\CampaignRelation[]
     */
    public function getCampaign()
    {
        return $this->offsetGet('campaign');
    }

    /**
     * Sets campaign
     *
     * @param \Data2CRMAPI\Model\CampaignRelation[] $campaign Campaign
     *
     * @return $this
     */
    public function setCampaign($campaign)
    {
        $this->offsetSet('campaign', $campaign);

        return $this;
    }
    /**
     * Gets project
     *
     * @return \Data2CRMAPI\Model\ProjectRelation[]
     */
    public function getProject()
    {
        return $this->offsetGet('project');
    }

    /**
     * Sets project
     *
     * @param \Data2CRMAPI\Model\ProjectRelation[] $project Campaign
     *
     * @return $this
     */
    public function setProject($project)
    {
        $this->offsetSet('project', $project);

        return $this;
    }
    /**
     * Gets case
     *
     * @return \Data2CRMAPI\Model\CaseRelation[]
     */
    public function getCase()
    {
        return $this->offsetGet('case');
    }

    /**
     * Sets case
     *
     * @param \Data2CRMAPI\Model\CaseRelation[] $case Case
     *
     * @return $this
     */
    public function setCase($case)
    {
        $this->offsetSet('case', $case);

        return $this;
    }
}
