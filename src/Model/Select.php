<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class Select extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'label' => 'string',
        'value' => 'string'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'label' => 'label',
        'value' => 'value'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'label' => 'setLabel',
        'value' => 'setValue'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'label' => 'getLabel',
        'value' => 'getValue'
    );

    /**
     * Gets label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->offsetGet('label');
    }

    /**
     * Sets label
     *
     * @param string $label Label
     *
     * @return $this
     */
    public function setLabel($label)
    {
        $this->offsetSet('label', $label);

        return $this;
    }
    /**
     * Gets value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->offsetGet('value');
    }

    /**
     * Sets value
     *
     * @param string $value Value
     *
     * @return $this
     */
    public function setValue($value)
    {
        $this->offsetSet('value', $value);

        return $this;
    }
}
