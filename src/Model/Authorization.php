<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class Authorization extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'type' => 'string',
        'description' => 'string',
        'platformCredential' => '\Data2CRMAPI\Model\PlatformCredential[]'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'type' => 'type',
        'description' => 'description',
        'platformCredential' => 'platform_credential'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'type' => 'setType',
        'description' => 'setDescription',
        'platformCredential' => 'setPlatformCredential'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'type' => 'getType',
        'description' => 'getDescription',
        'platformCredential' => 'getPlatformCredential'
    );

    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->offsetGet('type');
    }

    /**
     * Sets type
     *
     * @param string $type Type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->offsetSet('type', $type);

        return $this;
    }
    /**
     * Gets description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->offsetGet('description');
    }

    /**
     * Sets description
     *
     * @param string $description Description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->offsetSet('description', $description);

        return $this;
    }
    /**
     * Gets platformCredential
     *
     * @return \Data2CRMAPI\Model\PlatformCredential[]
     */
    public function getPlatformCredential()
    {
        return $this->offsetGet('platformCredential');
    }

    /**
     * Sets platformCredential
     *
     * @param \Data2CRMAPI\Model\PlatformCredential[] $platformCredential Credential
     *
     * @return $this
     */
    public function setPlatformCredential($platformCredential)
    {
        $this->offsetSet('platformCredential', $platformCredential);

        return $this;
    }
}
