<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class PlatformEntity extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'type' => 'string',
        'name' => 'string',
        'authorization' => '\Data2CRMAPI\Model\Authorization[]',
        'resource' => '\Data2CRMAPI\Model\Resource'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'type' => 'type',
        'name' => 'name',
        'authorization' => 'authorization',
        'resource' => 'resource'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'type' => 'setType',
        'name' => 'setName',
        'authorization' => 'setAuthorization',
        'resource' => 'setResource'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'type' => 'getType',
        'name' => 'getName',
        'authorization' => 'getAuthorization',
        'resource' => 'getResource'
    );

    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->offsetGet('type');
    }

    /**
     * Sets type
     *
     * @param string $type Type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->offsetSet('type', $type);

        return $this;
    }
    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->offsetGet('name');
    }

    /**
     * Sets name
     *
     * @param string $name Name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->offsetSet('name', $name);

        return $this;
    }
    /**
     * Gets authorization
     *
     * @return \Data2CRMAPI\Model\Authorization[]
     */
    public function getAuthorization()
    {
        return $this->offsetGet('authorization');
    }

    /**
     * Sets authorization
     *
     * @param \Data2CRMAPI\Model\Authorization[] $authorization Authorization
     *
     * @return $this
     */
    public function setAuthorization($authorization)
    {
        $this->offsetSet('authorization', $authorization);

        return $this;
    }
    /**
     * Gets resource
     *
     * @return \Data2CRMAPI\Model\Resource
     */
    public function getResource()
    {
        return $this->offsetGet('resource');
    }

    /**
     * Sets resource
     *
     * @param \Data2CRMAPI\Model\Resource $resource Platform resources
     *
     * @return $this
     */
    public function setResource($resource)
    {
        $this->offsetSet('resource', $resource);

        return $this;
    }
}
