<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class CustomFieldEntity extends AbstractModel  implements ArrayAccess
{   const TYPE_INTEGER = 'integer';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_DECIMAL = 'decimal';
    const TYPE_STRING = 'string';
    const TYPE_DATE = 'date';
    const TYPE_TIME = 'time';
    const TYPE_DATETIME = 'datetime';
    const TYPE_ARRAY = 'array';
    const TYPE_SELECT = 'select';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_RADIO = 'radio';
    
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'id' => 'string',
        'entity' => 'string',
        'label' => 'string',
        'required' => 'bool',
        'name' => 'string',
        'description' => 'string',
        'type' => 'string',
        'select' => '\Data2CRMAPI\Model\Select[]',
        'length' => 'int',
        'decimalPlaces' => 'int',
        'defaultValue' => 'string'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'id' => 'id',
        'entity' => 'entity',
        'label' => 'label',
        'required' => 'required',
        'name' => 'name',
        'description' => 'description',
        'type' => 'type',
        'select' => 'select',
        'length' => 'length',
        'decimalPlaces' => 'decimal_places',
        'defaultValue' => 'default_value'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'id' => 'setId',
        'entity' => 'setEntity',
        'label' => 'setLabel',
        'required' => 'setRequired',
        'name' => 'setName',
        'description' => 'setDescription',
        'type' => 'setType',
        'select' => 'setSelect',
        'length' => 'setLength',
        'decimalPlaces' => 'setDecimalPlaces',
        'defaultValue' => 'setDefaultValue'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'id' => 'getId',
        'entity' => 'getEntity',
        'label' => 'getLabel',
        'required' => 'getRequired',
        'name' => 'getName',
        'description' => 'getDescription',
        'type' => 'getType',
        'select' => 'getSelect',
        'length' => 'getLength',
        'decimalPlaces' => 'getDecimalPlaces',
        'defaultValue' => 'getDefaultValue'
    );

    /**
     * Gets id
     *
     * @return string
     */
    public function getId()
    {
        return $this->offsetGet('id');
    }

    /**
     * Sets id
     *
     * @param string $id Custom Field Identifier
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->offsetSet('id', $id);

        return $this;
    }
    /**
     * Gets entity
     *
     * @return string
     */
    public function getEntity()
    {
        return $this->offsetGet('entity');
    }

    /**
     * Sets entity
     *
     * @param string $entity Entity
     *
     * @return $this
     */
    public function setEntity($entity)
    {
        $this->offsetSet('entity', $entity);

        return $this;
    }
    /**
     * Gets label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->offsetGet('label');
    }

    /**
     * Sets label
     *
     * @param string $label Label
     *
     * @return $this
     */
    public function setLabel($label)
    {
        $this->offsetSet('label', $label);

        return $this;
    }
    /**
     * Gets required
     *
     * @return bool
     */
    public function getRequired()
    {
        return $this->offsetGet('required');
    }

    /**
     * Sets required
     *
     * @param bool $required Required
     *
     * @return $this
     */
    public function setRequired($required)
    {
        $this->offsetSet('required', $required);

        return $this;
    }
    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->offsetGet('name');
    }

    /**
     * Sets name
     *
     * @param string $name Name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->offsetSet('name', $name);

        return $this;
    }
    /**
     * Gets description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->offsetGet('description');
    }

    /**
     * Sets description
     *
     * @param string $description Description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->offsetSet('description', $description);

        return $this;
    }
    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->offsetGet('type');
    }

    /**
     * Sets type
     *
     * @param string $type Type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->offsetSet('type', $type);

        return $this;
    }
    /**
     * Gets select
     *
     * @return \Data2CRMAPI\Model\Select[]
     */
    public function getSelect()
    {
        return $this->offsetGet('select');
    }

    /**
     * Sets select
     *
     * @param \Data2CRMAPI\Model\Select[] $select Select Pairs for field with type \"select\"
     *
     * @return $this
     */
    public function setSelect($select)
    {
        $this->offsetSet('select', $select);

        return $this;
    }
    /**
     * Gets length
     *
     * @return int
     */
    public function getLength()
    {
        return $this->offsetGet('length');
    }

    /**
     * Sets length
     *
     * @param int $length Length
     *
     * @return $this
     */
    public function setLength($length)
    {
        $this->offsetSet('length', $length);

        return $this;
    }
    /**
     * Gets decimalPlaces
     *
     * @return int
     */
    public function getDecimalPlaces()
    {
        return $this->offsetGet('decimalPlaces');
    }

    /**
     * Sets decimalPlaces
     *
     * @param int $decimalPlaces Decimal Places
     *
     * @return $this
     */
    public function setDecimalPlaces($decimalPlaces)
    {
        $this->offsetSet('decimalPlaces', $decimalPlaces);

        return $this;
    }
    /**
     * Gets defaultValue
     *
     * @return string
     */
    public function getDefaultValue()
    {
        return $this->offsetGet('defaultValue');
    }

    /**
     * Sets defaultValue
     *
     * @param string $defaultValue Default Value
     *
     * @return $this
     */
    public function setDefaultValue($defaultValue)
    {
        $this->offsetSet('defaultValue', $defaultValue);

        return $this;
    }
}
