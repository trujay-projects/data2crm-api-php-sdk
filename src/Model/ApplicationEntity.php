<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class ApplicationEntity extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'key' => 'string',
        'type' => 'string',
        'authorization' => 'string',
        'description' => 'string'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'key' => 'key',
        'type' => 'type',
        'authorization' => 'authorization',
        'description' => 'description'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'key' => 'setKey',
        'type' => 'setType',
        'authorization' => 'setAuthorization',
        'description' => 'setDescription'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'key' => 'getKey',
        'type' => 'getType',
        'authorization' => 'getAuthorization',
        'description' => 'getDescription'
    );

    /**
     * Gets key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->offsetGet('key');
    }

    /**
     * Sets key
     *
     * @param string $key Application key
     *
     * @return $this
     */
    public function setKey($key)
    {
        $this->offsetSet('key', $key);

        return $this;
    }
    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->offsetGet('type');
    }

    /**
     * Sets type
     *
     * @param string $type Application platform type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->offsetSet('type', $type);

        return $this;
    }
    /**
     * Gets authorization
     *
     * @return string
     */
    public function getAuthorization()
    {
        return $this->offsetGet('authorization');
    }

    /**
     * Sets authorization
     *
     * @param string $authorization Application authorization
     *
     * @return $this
     */
    public function setAuthorization($authorization)
    {
        $this->offsetSet('authorization', $authorization);

        return $this;
    }
    /**
     * Gets description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->offsetGet('description');
    }

    /**
     * Sets description
     *
     * @param string $description Application description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->offsetSet('description', $description);

        return $this;
    }
}
