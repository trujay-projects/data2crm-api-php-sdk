<?php

namespace Data2CRMAPI\Model;

use \ArrayAccess;

class AttachmentEntity extends AbstractModel  implements ArrayAccess
{   
    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'id' => 'string',
        'title' => 'string',
        'description' => 'string',
        'name' => 'string',
        'mimeType' => 'string',
        'size' => 'int',
        'parent' => '\Data2CRMAPI\Model\AttachmentEntityRelation',
        'relation' => '\Data2CRMAPI\Model\Relation',
        'assignedUser' => '\Data2CRMAPI\Model\UserEntityRelation',
        'user' => '\Data2CRMAPI\Model\UserEntityRelation',
        'createdBy' => '\Data2CRMAPI\Model\UserEntityRelation',
        'createdAt' => '\DateTime',
        'updatedBy' => '\Data2CRMAPI\Model\UserEntityRelation',
        'updatedAt' => '\DateTime'
    );

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = array(
        'id' => 'id',
        'title' => 'title',
        'description' => 'description',
        'name' => 'name',
        'mimeType' => 'mime_type',
        'size' => 'size',
        'parent' => 'parent',
        'relation' => 'relation',
        'assignedUser' => 'assigned_user',
        'user' => 'user',
        'createdBy' => 'created_by',
        'createdAt' => 'created_at',
        'updatedBy' => 'updated_by',
        'updatedAt' => 'updated_at'
    );

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = array(
        'id' => 'setId',
        'title' => 'setTitle',
        'description' => 'setDescription',
        'name' => 'setName',
        'mimeType' => 'setMimeType',
        'size' => 'setSize',
        'parent' => 'setParent',
        'relation' => 'setRelation',
        'assignedUser' => 'setAssignedUser',
        'user' => 'setUser',
        'createdBy' => 'setCreatedBy',
        'createdAt' => 'setCreatedAt',
        'updatedBy' => 'setUpdatedBy',
        'updatedAt' => 'setUpdatedAt'
    );

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = array(
        'id' => 'getId',
        'title' => 'getTitle',
        'description' => 'getDescription',
        'name' => 'getName',
        'mimeType' => 'getMimeType',
        'size' => 'getSize',
        'parent' => 'getParent',
        'relation' => 'getRelation',
        'assignedUser' => 'getAssignedUser',
        'user' => 'getUser',
        'createdBy' => 'getCreatedBy',
        'createdAt' => 'getCreatedAt',
        'updatedBy' => 'getUpdatedBy',
        'updatedAt' => 'getUpdatedAt'
    );

    /**
     * Gets id
     *
     * @return string
     */
    public function getId()
    {
        return $this->offsetGet('id');
    }

    /**
     * Sets id
     *
     * @param string $id Note Identifier
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->offsetSet('id', $id);

        return $this;
    }
    /**
     * Gets title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->offsetGet('title');
    }

    /**
     * Sets title
     *
     * @param string $title Title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->offsetSet('title', $title);

        return $this;
    }
    /**
     * Gets description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->offsetGet('description');
    }

    /**
     * Sets description
     *
     * @param string $description Description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->offsetSet('description', $description);

        return $this;
    }
    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->offsetGet('name');
    }

    /**
     * Sets name
     *
     * @param string $name Name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->offsetSet('name', $name);

        return $this;
    }
    /**
     * Gets mimeType
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->offsetGet('mimeType');
    }

    /**
     * Sets mimeType
     *
     * @param string $mimeType Mime Type
     *
     * @return $this
     */
    public function setMimeType($mimeType)
    {
        $this->offsetSet('mimeType', $mimeType);

        return $this;
    }
    /**
     * Gets size
     *
     * @return int
     */
    public function getSize()
    {
        return $this->offsetGet('size');
    }

    /**
     * Sets size
     *
     * @param int $size Size (in bytes)
     *
     * @return $this
     */
    public function setSize($size)
    {
        $this->offsetSet('size', $size);

        return $this;
    }
    /**
     * Gets parent
     *
     * @return \Data2CRMAPI\Model\AttachmentEntityRelation
     */
    public function getParent()
    {
        return $this->offsetGet('parent');
    }

    /**
     * Sets parent
     *
     * @param \Data2CRMAPI\Model\AttachmentEntityRelation $parent Parent
     *
     * @return $this
     */
    public function setParent($parent)
    {
        $this->offsetSet('parent', $parent);

        return $this;
    }
    /**
     * Gets relation
     *
     * @return \Data2CRMAPI\Model\Relation
     */
    public function getRelation()
    {
        return $this->offsetGet('relation');
    }

    /**
     * Sets relation
     *
     * @param \Data2CRMAPI\Model\Relation $relation Relation
     *
     * @return $this
     */
    public function setRelation($relation)
    {
        $this->offsetSet('relation', $relation);

        return $this;
    }
    /**
     * Gets assignedUser
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getAssignedUser()
    {
        return $this->offsetGet('assignedUser');
    }

    /**
     * Sets assignedUser
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $assignedUser Assigned User
     *
     * @return $this
     */
    public function setAssignedUser($assignedUser)
    {
        $this->offsetSet('assignedUser', $assignedUser);

        return $this;
    }
    /**
     * Gets user
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getUser()
    {
        return $this->offsetGet('user');
    }

    /**
     * Sets user
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $user User
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->offsetSet('user', $user);

        return $this;
    }
    /**
     * Gets createdBy
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getCreatedBy()
    {
        return $this->offsetGet('createdBy');
    }

    /**
     * Sets createdBy
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $createdBy Created By
     *
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->offsetSet('createdBy', $createdBy);

        return $this;
    }
    /**
     * Gets createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->offsetGet('createdAt');
    }

    /**
     * Sets createdAt
     *
     * @param \DateTime $createdAt Created At
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->offsetSet('createdAt', $createdAt);

        return $this;
    }
    /**
     * Gets updatedBy
     *
     * @return \Data2CRMAPI\Model\UserEntityRelation
     */
    public function getUpdatedBy()
    {
        return $this->offsetGet('updatedBy');
    }

    /**
     * Sets updatedBy
     *
     * @param \Data2CRMAPI\Model\UserEntityRelation $updatedBy Updated By
     *
     * @return $this
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->offsetSet('updatedBy', $updatedBy);

        return $this;
    }
    /**
     * Gets updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->offsetGet('updatedAt');
    }

    /**
     * Sets updatedAt
     *
     * @param \DateTime $updatedAt Updated At
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->offsetSet('updatedAt', $updatedAt);

        return $this;
    }
}
