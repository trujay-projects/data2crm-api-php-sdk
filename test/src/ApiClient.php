<?php

namespace Data2CRMAPITest\src;

use Data2CRMAPI\ApiClient as BaseApiClient;
use Data2CRMAPI\Data;

class ApiClient extends BaseApiClient
{
    /**
     * @var array
     */
    protected $callMock = [];

    /**
     * @param string $path
     * @param string $method
     * @param array $query
     * @param array $post
     * @param array $headers
     * @param string $responseType
     * @param Data $data
     * 
     * @return array
     * 
     * @throws \Data2CRMAPI\ApiException
     * @throws \Exception
     */
    public function call($path, $method, $query, $post, $headers, $responseType, Data $data)
    {
        if ($this->callMock['arguments'][0] !== $path) {
            throw new \Exception(
                sprintf(
                    'Path expected "%s", actual: "%s"',
                    var_export($this->callMock['arguments'][0], true),
                    var_export($path, true)
                )
            );
        }

        if ($this->callMock['arguments'][1] !== $method) {
            throw new \Exception(
                sprintf(
                    'Method expected "%s", actual: "%s"',
                    var_export($this->callMock['arguments'][1], true),
                    var_export($method, true)
                )
            );
        }

        if ($this->callMock['arguments'][2] !== $query) {
            throw new \Exception(
                sprintf(
                    'Query expected "%s", actual: "%s"',
                    var_export($this->callMock['arguments'][2], true),
                    var_export($query, true)
                )
            );
        }

        if ($this->callMock['arguments'][3] !== $post) {
            throw new \Exception(
                sprintf(
                    'Post expected "%s", actual: "%s"',
                    var_export($this->callMock['arguments'][3], true),
                    var_export($post, true)
                )
            );
        }

        if ($this->callMock['arguments'][4] !== $headers) {
            throw new \Exception(
                sprintf(
                    'Headers expected "%s", actual: "%s"',
                    var_export($this->callMock['arguments'][4], true),
                    var_export($headers, true)
                )
            );
        }

        if ($this->callMock['arguments'][5] !== $responseType) {
            throw new \Exception(
                sprintf(
                    'Response type expected "%s", actual: "%s"',
                    var_export($this->callMock['arguments'][5], true),
                    var_export($responseType, true)
                )
            );
        }

        if ($this->callMock['arguments'][6] !== serialize($data)) {
            throw new \Exception(
                sprintf(
                    'Response type expected "%s", actual: "%s"',
                    $this->callMock['arguments'][6],
                    json_encode(serialize($data), true)
                )
            );
        }

        return $this->readResponse(
            $method, 
            'https://test.com/',
            $this->callMock['result']['info'],
            $this->callMock['result']['headers'],
            json_encode($this->callMock['result']['body']),
            $responseType, 
            $data
        );
    }

    /**
     * @param array $arguments
     * @param array $result
     */
    public function setCallMock(array $arguments, array $result)
    {
        $this->callMock = array(
            'arguments' => $arguments,
            'result' => $result
        );
    }
}
